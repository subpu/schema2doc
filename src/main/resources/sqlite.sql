/*
 *  Create & init site content
 */

-- ITEMS --

CREATE TABLE IF NOT EXISTS sys_items (
  id INTEGER PRIMARY KEY,
  name TEXT DEFAULT "",
  unit TEXT DEFAULT "",
  price INTEGER DEFAULT 0,
  qty INTEGER DEFAULT 0,
  desc TEXT DEFAULT ""
);

-- ANNOUNCEMENTS --

CREATE TABLE IF NOT EXISTS sys_announce (
  id INTEGER PRIMARY KEY,
  title TEXT DEFAULT "",
  date TEXT DEFAULT "",
  body TEXT DEFAULT ""
);

-- EVENTS --

CREATE TABLE IF NOT EXISTS sys_events (
  id INTEGER PRIMARY KEY,
  title TEXT DEFAULT "",
  date TEXT DEFAULT "",
  body TEXT DEFAULT ""
);

-- MOTD --

CREATE TABLE IF NOT EXISTS msg_motd (
  id INTEGER PRIMARY KEY,
  title TEXT DEFAULT "",
  body TEXT DEFAULT ""
);
