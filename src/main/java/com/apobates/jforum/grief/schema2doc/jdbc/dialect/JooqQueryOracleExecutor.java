package com.apobates.jforum.grief.schema2doc.jdbc.dialect;

import com.apobates.jforum.grief.schema2doc.core.SchemaTableStrategy;
import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Nullable;
import com.apobates.jforum.grief.schema2doc.core.entity.PrimayKey;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.jdbc.schema.AbstractQueryExecutor;
import com.apobates.jforum.grief.schema2doc.core.schema.SchemaDialect;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * Oracle的结果集供应商
 */
public class JooqQueryOracleExecutor extends AbstractQueryExecutor {
    private final static Logger logger = LoggerFactory.getLogger(JooqQueryOracleExecutor.class);

    public JooqQueryOracleExecutor(DataSource dataSource, SchemaDialect dialect) {
        super(dataSource, dialect);
    }

    @Override
    protected Stream<Table> getTables(DSLContext context, Optional<String> dbName, Optional<String> schema, SchemaTableStrategy tableStrategy) throws IllegalArgumentException {
        // SELECT TABLE_NAME,COMMENTS AS REMARKS FROM USER_TAB_COMMENTS WHERE TABLE_TYPE = 'TABLE'
        // DBA: sql = "SELECT TABLE_NAME,COMMENTS AS REMARKS FROM DBA_TAB_COMMENTS WHERE TABLE_TYPE = 'TABLE' AND OWNER = '"+ getSchema() + "'";
        String sql = "SELECT t.TABLE_NAME,tc.COMMENTS FROM ALL_TABLES t LEFT JOIN ALL_TAB_COMMENTS tc ON tc.TABLE_NAME = t.TABLE_NAME and tc.TABLE_TYPE = 'TABLE' WHERE t.owner = :schema ";
                // "SELECT ORIGIN_CON_ID, TABLE_NAME, COMMENTS FROM USER_TAB_COMMENTS WHERE TABLE_TYPE = 'TABLE'";
        if(tableStrategy.sql(SchemaDialect.Oracle).isPresent()){
            sql+= " AND " + tableStrategy.sql(SchemaDialect.Oracle).get().replaceAll(SchemaTableStrategy.placeholder, "t.TABLE_NAME");
        }
        logger.debug("[TABLE-Oracle]execute sql statement::"+sql);
        return context.fetch(sql, DSL.param("schema", schema.get())).stream().map(record -> {
            // String dbName, String tableName, String remarks
            return Table.noDatabase(
                    0,
                    schema.get(),
                    record.get("TABLE_NAME", String.class),
                    record.get("COMMENTS", String.class));
        }).filter(table->tableStrategy.filter().test(table.getTableName()));
    }
    // https://docs.oracle.com/en/database/oracle/oracle-database/19/refrn/ALL_TAB_COMMENTS.html#GUID-699B7976-C1DF-4502-A70C-7DDEBDBF3127
    // https://docs.oracle.com/en/database/oracle/oracle-database/19/refrn/ALL_TAB_COLUMNS.html#GUID-F218205C-7D76-4A83-8691-BFD2AD372B63
    // https://docs.oracle.com/en/database/oracle/oracle-database/19/refrn/ALL_TABLES.html#GUID-6823CD28-0681-468E-950B-966C6F71325D
    @Override
    protected Stream<Column> getColumns(DSLContext context, Table table) {
        // sql = "SELECT ut.TABLE_NAME, ut.COLUMN_NAME, uc.comments as REMARKS, concat(concat(concat(ut.DATA_TYPE, '('), ut.DATA_LENGTH), ')') AS COLUMN_TYPE, ut.DATA_LENGTH as COLUMN_LENGTH
        // FROM user_tab_columns ut
        // INNER JOIN user_col_comments uc ON ut.TABLE_NAME = uc.table_name AND ut.COLUMN_NAME = uc.column_name
        // WHERE ut.Table_Name = '%s'";
        final List<String> pks = getPrimayKey(context, table.getTableName());
        String sql= "SELECT ut.COLUMN_NAME, uc.COMMENTS as REMARKS, concat(concat(concat(ut.DATA_TYPE, '('), ut.DATA_LENGTH), ')') AS COLUMN_TYPE, ut.DATA_LENGTH as COLUMN_LENGTH, ut.NULLABLE, ut.DATA_DEFAULT, ut.DATA_SCALE FROM ALL_TAB_COLUMNS ut LEFT JOIN ALL_COL_COMMENTS uc ON ut.TABLE_NAME = uc.table_name AND ut.COLUMN_NAME = uc.column_name WHERE ut.Table_Name = :tablename";
                // "SELECT ut.COLUMN_NAME, uc.comments as REMARKS, concat(concat(concat(ut.DATA_TYPE, '('), ut.DATA_LENGTH), ')') AS COLUMN_TYPE, ut.DATA_LENGTH as COLUMN_LENGTH, ut.NULLABLE, ut.DATA_DEFAULT, ut.DATA_SCALE FROM user_tab_columns ut INNER JOIN user_col_comments uc ON ut.TABLE_NAME = uc.table_name AND ut.COLUMN_NAME = uc.column_name WHERE ut.Table_Name = :tablename ";
        System.out.println(sql);
        // Fetch results using jOOQ
        return context.fetch(sql, DSL.param("tablename", table.getTableName())).stream().map(record -> {
            // Basic:: String columnName, String typeName, String columnSize, Nullable nullable, String remarks
            String columnName = record.get("COLUMN_NAME", String.class);
            return Column.basic(
                            columnName,
                            record.get("COLUMN_TYPE", String.class),
                            record.get("COLUMN_LENGTH", String.class),
                            Nullable.of(record.get("NULLABLE", String.class).equals("Y")?1:2),
                            record.get("REMARKS", String.class))
                    .toFull(record.get("DATA_SCALE", String.class),
                            pks.contains(columnName)?PrimayKey.YES:PrimayKey.NO,
                            record.get("DATA_DEFAULT", String.class)); // String decimalDigits, PrimayKey pk, String columnDef
        });
    }

    private List<String> getPrimayKey(DSLContext context, String tableName){
        String sql = "SELECT C.COLUMN_NAME, C.POSITION AS KEY_SEQ, C.CONSTRAINT_NAME AS PK_NAME FROM ALL_CONS_COLUMNS C, ALL_CONSTRAINTS K WHERE K.CONSTRAINT_TYPE = 'P' AND K.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND K.TABLE_NAME = C.TABLE_NAME AND K.OWNER = C.OWNER AND C.TABLE_NAME = :TN";
        return context.fetch(sql, DSL.param("TN", tableName)).stream().map(record -> {
            return record.get("COLUMN_NAME", String.class);
        }).collect(Collectors.toList());
    }
}
