package com.apobates.jforum.grief.schema2doc.output.freeMarker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import java.io.IOException;

/**
 * freemarker内置模板的基类
 */
public abstract class ExportTemplate {
    public final String version;
    private final String encoding;

    protected ExportTemplate(String version, String encoding) {
        this.version = version;
        this.encoding = encoding;
    }

    public Template get() throws IOException {
        Configuration cfg = new Configuration(new Version(version));

        cfg.setClassForTemplateLoading(ExportTemplate.class, "/");
        cfg.setDefaultEncoding(encoding);

        return cfg.getTemplate(getNames());
    }

    /**
     * 模板的名称
     * @return
     */
    protected abstract String getNames();

    /**
     * 输出的列名称数组, 叁看Column的字段名称
     * 用于: 判断某些列的格式化
     * @return 不限名称索引大小/忽略出显顺序
     */
    public abstract String[] outputColumn();

    /**
     * 基本模式
     * @return
     */
    public static ExportTemplate basic(){
        return new ExportTemplate("2.3.30", "UTF-8") {
            @Override
            protected String getNames() {
                return "documentation_word.basic.ftl";
            }

            @Override
            public String[] outputColumn() {
                return new String[]{"columnName", "typeName", "columnSize", "nullable", "remarks"};
            }
        };
    }

    /**
     * 全模式
     * @return
     */
    public static ExportTemplate full(){
        return new ExportTemplate("2.3.30", "UTF-8") {
            @Override
            protected String getNames() {
                return "documentation_word.full.ftl";
            }

            @Override
            public String[] outputColumn() {
                return new String[]{"columnName", "typeName", "columnSize", "nullable", "remarks", "decimalDigits", "primaryKey", "columnDef"};
            }
        };
    }
}
