package com.apobates.jforum.grief.schema2doc.output.docx;

import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;
import java.math.BigInteger;

/**
 * 抽像的Docx4J写入
 */
public abstract class AbstractDocxWordOutput {

    public abstract int getWriteSize();

    protected void addElement(MainDocumentPart mainDocumentPart, Table table){
        if(null == table || table.getColumns().isEmpty()){
            return;
        }
        int rowSize = table.getColumns().size();
        mainDocumentPart.addParagraphOfText(String.format("表名:%s(%s)", table.getTableName(), table.getRemarks()));
        // int writableWidthTwips = wordPackage.getDocumentModel().getSections().get(0).getPageDimensions().getWritableWidthTwips();
        ObjectFactory objectFactory =  Context.getWmlObjectFactory();
        Tbl tbl = objectFactory.createTbl();
        PPr ppr = new PPr();
        Jc jc = new Jc();
        jc.setVal(JcEnumeration.CENTER);
        ppr.setJc(jc);//单元格文本居中使用
        // 表头
        Tr theadTr = objectFactory.createTr();
        for (int j = 0; j < 9; j++) { // 列
            Tc tc = objectFactory.createTc();
            tc.setTcPr(new TcPr());
            tc.getContent().add(mainDocumentPart.createParagraphOfText(getTableTitle(j)));
            theadTr.getContent().add(tc);
        }
        tbl.getContent().add(theadTr);
        // 内容
        for (int i=1; i<=rowSize; i++){ // 行
            Tr dataTr = objectFactory.createTr();
            Column c = table.getColumns().get(i-1);
            for (int j = 0; j < 9; j++) { // 列
                Tc tc = objectFactory.createTc();
                tc.setTcPr(new TcPr());
                tc.getContent().add(mainDocumentPart.createParagraphOfText(getTableTdVal(c, j, i)));
                dataTr.getContent().add(tc);
            }
            tbl.getContent().add(dataTr);
        }
        setTcBorders(tbl);
        mainDocumentPart.addObject(tbl);
        // PPrBase.Ind pPrBaseInd = objectFactory.createPPrBaseInd();
        // pPrBaseInd.setLeft(BigInteger.valueOf(720));
        // mainDocumentPart.getContent().add(pPrBaseInd);

        mainDocumentPart.addParagraphOfText("\r\n");
    }
    /**
     * 设置单元格边框
     * @param table
     */
    private static void setTcBorders(Tbl table) {
        table.setTblPr(new TblPr());// 必须设置一个TblPr，否则最后会报空指针异常
        CTBorder border = new CTBorder();
        border.setColor("auto");
        border.setSz(new BigInteger("1"));
        border.setSpace(new BigInteger("0"));
        border.setVal(STBorder.SINGLE);
        TblBorders borders = new TblBorders();
        borders.setBottom(border);
        borders.setLeft(border);
        borders.setRight(border);
        borders.setTop(border);
        borders.setInsideH(border);
        borders.setInsideV(border);
        // 获取其内部的TblPr属性设置属性
        table.getTblPr().setTblBorders(borders);
    }
    protected String getTableTitle(int tdIndex){
        String data = "序号";
        switch (tdIndex){
            case 1 -> data = "列名";
            case 2 -> data = "数据类型";
            case 3 -> data = "长度";
            case 4 -> data = "小数位";
            case 5 -> data = "允许空值";
            case 6 -> data = "主键";
            case 7 -> data = "默认值";
            case 8 -> data = "备注";
        }
        return data;
    }
    protected String getTableTdVal(Column column, int tdIndex, int trIndex){
        String data = String.valueOf(trIndex);
        switch (tdIndex){
            case 1 -> data = column.getColumnName();
            case 2 -> data = column.getTypeName();
            case 3 -> data = column.getColumnSize();
            case 4 -> data = column.getDecimalDigits();
            case 5 -> data = column.getNullable();
            case 6 -> data = column.getPrimaryKey();
            case 7 -> data = column.getColumnDef();
            case 8 -> data = column.getRemarks();
        }
        return data;
    }
}
