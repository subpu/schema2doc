package com.apobates.jforum.grief.schema2doc.core.entity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 表名信息(tables)
 */
public final class Table {
    private final int id;
    // 数据名称
    private final String dbName;
    // 模式名称
    private final String schema;
    // 表名
    private final String tableName;
    // 备注
    private final String remarks;
    // 列信息
    private final List<Column> columns;

    private Table(int id, String dbName, String schema, String tableName, String remarks, List<Column> columns) {
        this.id = id;
        this.dbName = dbName;
        this.tableName = tableName;
        this.remarks = remarks;
        this.columns = columns;
        this.schema = schema;
    }

    /**
     * 适用于有模式名称和数据库名称概述的方言
     * @param id 表ID
     * @param dbName 数据库名称
     * @param schema 模式名称
     * @param tableName 表名
     * @param remarks 表的注释
     */
    public Table(int id,String dbName, String schema, String tableName, String remarks) {
        this.id = id;
        this.dbName = dbName;
        this.tableName = tableName;
        this.remarks = remarks;
        this.columns = Collections.emptyList();
        this.schema = schema;
    }

    /**
     * 适用于没有模式名称概述的方言
     * @param id 表ID
     * @param dbName 数据库名称
     * @param tableName 表名
     * @param remarks 表的注释
     * @return
     */
    public static Table noSchema(int id, String dbName, String tableName, String remarks) {
        return new Table(id, dbName, dbName, tableName, remarks);
    }

    /**
     * 适用于没有数据库概述的方言
     * @param id 表ID
     * @param schema 模式名称
     * @param tableName 表名
     * @param remarks 表的注释
     * @return
     */
    public static Table noDatabase(int id, String schema, String tableName, String remarks) {
        return new Table(id, schema, schema, tableName, remarks);
    }

    public String getSchema() {
        return schema;
    }

    public String getDbName() {
        return dbName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getRemarks() {
        return remarks;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return tableName.equals(table.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableName);
    }

    public Table fillColumn(List<Column> columns){
        return new Table(getId(), getDbName(), getSchema(), getTableName(), getRemarks(), columns);
    }

    @Override
    public String toString() {
        return "Table{" +
                "dbName='" + dbName + '\'' +
                ", schema='" + schema + '\'' +
                ", tableName='" + tableName + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
