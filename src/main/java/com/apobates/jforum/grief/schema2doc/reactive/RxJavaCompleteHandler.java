package com.apobates.jforum.grief.schema2doc.reactive;

/**
 * RxJava Disposable后的回调
 */
@FunctionalInterface
public interface RxJavaCompleteHandler {
    /**
     * 回调方法
     */
    void apply();
}
