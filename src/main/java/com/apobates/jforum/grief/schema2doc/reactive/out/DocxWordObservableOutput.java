package com.apobates.jforum.grief.schema2doc.reactive.out;

import com.apobates.jforum.grief.schema2doc.SchemaExportException;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.output.docx.AbstractDocxWordOutput;
import com.apobates.jforum.grief.schema2doc.reactive.SchemaObservableOutput;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class DocxWordObservableOutput extends AbstractDocxWordOutput implements SchemaObservableOutput {
    private final String outputPath;
    private volatile CompletableFuture<Integer> future = new CompletableFuture<>();
    private final WordprocessingMLPackage wordPackage;
    private final MainDocumentPart mainDocumentPart;

    private int total = 0;
    private final static Logger logger = LoggerFactory.getLogger(DocxWordObservableOutput.class);

    public DocxWordObservableOutput(String outputPath) {
        this.outputPath = outputPath;
        try {
            this.wordPackage = WordprocessingMLPackage.createPackage();
            this.mainDocumentPart = wordPackage.getMainDocumentPart();
        } catch (InvalidFormatException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Disposable flush(Information information, Observable<Table> table) throws SchemaExportException {
        List<Table> rs = new ArrayList<>();
        DisposableObserver<Table> disposableObserver = table.subscribeWith(new DisposableObserver<Table>() {
            @Override
            public void onNext(@NonNull Table tableIns) {
                rs.add(tableIns);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                logger.debug("Export Break, reason: " + throwable.getMessage());
                future.cancel(false);
                throw new SchemaExportException(throwable);
            }

            @Override
            public void onComplete() {
                logger.debug("Export Complete");
                try {
                    total = rs.size();
                    rs.stream().forEach(table->addElement(mainDocumentPart, table));
                    // 写入文件
                    String outputFilePath = String.format("%s/%d.docx", outputPath, System.currentTimeMillis());
                    File exportFile = new File(outputFilePath);
                    wordPackage.save(exportFile);
                    future.complete(total);
                } catch (Docx4JException ex) {
                    logger.debug("word save has exception, message: " + ex.getMessage());
                }
            }
        });
        return disposableObserver;
    }

    @Override
    public CompletableFuture<Integer> getFuture() {
        return future;
    }

    @Override
    public int getWriteSize() {
        return total;
    }

    @Override
    public String getActor() {
        return "Collector";
    }
}
