package com.apobates.jforum.grief.schema2doc.reactive;

import com.apobates.jforum.grief.schema2doc.core.SchemaTableStrategy;
import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import io.reactivex.Flowable;
import io.reactivex.Single;
import java.util.List;
import java.util.Optional;

/**
 * RxJava2 Flowable导出信息查询
 */
public interface SchemaQueryFlowableResult {
    /**
     * 导出的表
     * @param dbName
     * @param tableStrategy
     * @return
     */
    Flowable<Table> getAll(Optional<String> dbName, Optional<String> schema, SchemaTableStrategy tableStrategy);

    /**
     * 某表导出的列
     * @param table
     * @return
     */
    Single<List<Column>> getTableColumn(Table table);
}
