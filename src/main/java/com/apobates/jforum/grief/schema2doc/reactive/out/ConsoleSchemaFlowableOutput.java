package com.apobates.jforum.grief.schema2doc.reactive.out;

import com.apobates.jforum.grief.schema2doc.SchemaExportException;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.reactive.SchemaFlowableOutput;
import com.github.freva.asciitable.AsciiTable;
import com.github.freva.asciitable.Column;
import com.github.freva.asciitable.OverflowBehaviour;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * RxJava2 Flowable的控制台打印
 */
public class ConsoleSchemaFlowableOutput implements SchemaFlowableOutput {
    private final static Logger logger = LoggerFactory.getLogger(ConsoleSchemaFlowableOutput.class);
    private volatile CompletableFuture<String> future = new CompletableFuture<>();
    private AtomicInteger count = new AtomicInteger(0);
    @Override
    public Disposable flush(Information information, Flowable<Table> table) throws SchemaExportException {
        logger.info("Start Flowable Flush");
        // dispose():主动解除订阅（如果使用Retrofit2+Rxjava2，调用dispose会主动取消网络请求，在本文的后半部分
        // .as(autoDisposable(ScopeProvider.UNBOUND)).
        Disposable export_flush_complete = table.subscribe(tableIns -> {
            // System.out.println("-------Information-------");
            // System.out.println(information);
            System.out.println(printAsciiTable(tableIns));
            System.out.println(printAsciiColumns(tableIns.getColumns()));
            System.out.println("\r\n");
            count.addAndGet(1);
        }, throwable -> {
            logger.debug("Export Break, reason: " + throwable.getMessage());
            // System.out.println("Export Break, reason: " + throwable.getMessage());
            future.cancel(true);
            throw new SchemaExportException(throwable);
        }, new Action() {
            @Override
            public void run() throws Exception {
                logger.debug("Export Complete, Affect Size:"+count.get());
                // System.out.println("Export Complete, Affect Size:"+count.get());
                future.complete("OK");
            }
        });
        return export_flush_complete;
    }

    @Override
    public CompletableFuture<String> getFuture() {
        return future;
    }

    private static String printAsciiTable(Table table){
        return AsciiTable.getTable(List.of(table), Arrays.asList(
                new Column().header("Table").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getTableName()),
                new Column().header("Remarks").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getRemarks()),
                new Column().header("ColumnSize").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getColumns().size()+"")
        ));
    }

    private static String printAsciiColumns(List<com.apobates.jforum.grief.schema2doc.core.entity.Column> columns){
        return AsciiTable.getTable(columns, Arrays.asList(
                new Column().header("Name").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnName()),
                new Column().header("Type").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getTypeName()),
                new Column().header("Length").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnSize()),
                new Column().header("Nullable").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getNullable()),
                new Column().header("Remarks").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getRemarks())));
    }
}
