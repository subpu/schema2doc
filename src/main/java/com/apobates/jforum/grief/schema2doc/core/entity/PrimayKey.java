package com.apobates.jforum.grief.schema2doc.core.entity;

/**
 * 是否是主键
 */
public enum PrimayKey {
    YES(1),NO(2);

    private int symbol;


    PrimayKey(int symbol) {
        this.symbol = symbol;
    }

    public static PrimayKey of(Integer symbol){
        if(symbol.compareTo(1) == 0){
            return YES;
        }
        return NO;
    }

    public static PrimayKey getInstance(String names){
        if(names.equals("YES")){
            return YES;
        }
        return NO;
    }
}
