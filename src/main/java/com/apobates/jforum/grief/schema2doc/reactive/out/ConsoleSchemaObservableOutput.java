package com.apobates.jforum.grief.schema2doc.reactive.out;

import com.apobates.jforum.grief.schema2doc.SchemaExportException;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.reactive.SchemaObservableOutput;
import com.github.freva.asciitable.AsciiTable;
import com.github.freva.asciitable.Column;
import com.github.freva.asciitable.OverflowBehaviour;
import io.reactivex.*;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * RxJava2 Observable的控制台打印
 */
public class ConsoleSchemaObservableOutput implements SchemaObservableOutput {
    private final static Logger logger = LoggerFactory.getLogger(ConsoleSchemaObservableOutput.class);
    private volatile CompletableFuture<Integer> future = new CompletableFuture<>();
    private AtomicInteger count = new AtomicInteger(0);
    @Override
    public Disposable flush(Information information, Observable<Table> table) throws SchemaExportException {
        logger.info("Start Observable Flush");
        /* 只取request指定数量
        Subscriber<Table> subscriber = new Subscriber<Table>() {
            @Override
            public void onSubscribe(Subscription subscription) {
                System.out.println("Export Event join");
                subscription.request(10);
            }

            @Override
            public void onNext(Table table) {
                System.out.println(printAsciiTable(table));
                System.out.println(printAsciiColumns(table.getColumns()));
                System.out.println("\r\n");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.debug("Export Break, reason: "+throwable.getMessage());
                System.out.println("Export Break, reason: "+throwable.getMessage());
            }

            @Override
            public void onComplete() {
                logger.debug("Export Complete");
                System.out.println("Export Complete");

            }
        };
        table.subscribe(subscriber);*/

        DisposableObserver<Table> disposableObserver = table.subscribeWith(new DisposableObserver<Table>() {
            @Override
            public void onNext(@NonNull Table tableIns) {
                System.out.println(printAsciiTable(tableIns));
                System.out.println(printAsciiColumns(tableIns.getColumns()));
                System.out.println("\r\n");
                count.addAndGet(1);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                logger.debug("Export Break, reason: " + throwable.getMessage());
                // System.out.println("Export Break, reason: " + throwable.getMessage());
                future.cancel(false);
                throw new SchemaExportException(throwable);
            }

            @Override
            public void onComplete() {
                logger.debug("Export Complete");
                // System.out.println("Export Complete, Affect Size:"+count.get());
                future.complete(count.intValue());
            }
        });
        return disposableObserver;
        /*
        Observer<Table> observer = new Observer<Table>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                System.out.println("dispose:::"+d.isDisposed());
            }

            @Override
            public void onNext(@NonNull Table tableIns) {
                System.out.println(printAsciiTable(tableIns));
                System.out.println(printAsciiColumns(tableIns.getColumns()));
                System.out.println("\r\n");
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                logger.debug("Export Break, reason: "+throwable.getMessage());
                System.out.println("Export Break, reason: "+throwable.getMessage());
                throw new SchemaExportException(throwable);
            }

            @Override
            public void onComplete() {
                logger.debug("Export Complete");
                System.out.println("Export Complete");
            }
        };
        table.as(autoDisposable(ScopeProvider.UNBOUND)).subscribe(observer);*/
        // isDisposed():查询是否解除订阅 true 代表 已经解除订阅
        // return export_flush_complete;*/
    }

    @Override
    public CompletableFuture<Integer> getFuture() {
        return future;
    }

    @Override
    public String getActor() {
        return "Console";
    }

    private static String printAsciiTable(Table table){
        return AsciiTable.getTable(List.of(table), Arrays.asList(
                new Column().header("Table").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getTableName()),
                new Column().header("Remarks").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getRemarks()),
                new Column().header("ColumnSize").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getColumns().size()+"")
        ));
    }

    private static String printAsciiColumns(List<com.apobates.jforum.grief.schema2doc.core.entity.Column> columns){
        return AsciiTable.getTable(columns, Arrays.asList(
                new Column().header("Name").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnName()),
                new Column().header("Type").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getTypeName()),
                new Column().header("Length").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnSize()),
                new Column().header("Nullable").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getNullable()),
                new Column().header("Remarks").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getRemarks())));
    }
}
