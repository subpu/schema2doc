package com.apobates.jforum.grief.schema2doc.reactive.out;

import com.apobates.jforum.grief.schema2doc.SchemaExportException;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.output.docx.AbstractDocxWordOutput;
import com.apobates.jforum.grief.schema2doc.reactive.SchemaObservableOutput;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Observable.toList().subscribe
 * 可用/非异步
 */
public class DocxWordReactiveOutput extends AbstractDocxWordOutput implements SchemaObservableOutput {
    private final String outputPath;
    private volatile CompletableFuture<Integer> future = new CompletableFuture<>();
    private int total = 0;
    private final static Logger logger = LoggerFactory.getLogger(DocxWordReactiveOutput.class);

    public DocxWordReactiveOutput(String outputPath) {
        this.outputPath = outputPath;
    }

    @Override
    public int getWriteSize() {
        return total;
    }

    @Override
    public String getActor() {
        return "Single";
    }

    @Override
    public Disposable flush(Information information, Observable<Table> table) throws SchemaExportException {
        return table.toList().subscribe(new Consumer<List<Table>>() {
            @Override
            public void accept(List<Table> tables) throws Exception {
                WordprocessingMLPackage wordPackage = WordprocessingMLPackage.createPackage();
                MainDocumentPart mainDocumentPart = wordPackage.getMainDocumentPart();
                total = tables.size();
                //
                tables.forEach(table -> addElement(mainDocumentPart, table));
                String outputFilePath = String.format("%s/%d.docx", outputPath, System.currentTimeMillis());
                File exportFile = new File(outputFilePath);
                wordPackage.save(exportFile);
                future.complete(total);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                future.cancel(false);
            }
        });
    }

    @Override
    public CompletableFuture<Integer> getFuture() {
        return future;
    }
}
