package com.apobates.jforum.grief.schema2doc.core;

import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * 导出信息查询
 */
public interface SchemaQueryResult {
    /**
     * 导出的表
     * @param dbName 数据库名称
     * @param schema 模式名称
     * @param tableStrategy 表名策略
     * @return
     * @throws SQLException
     */
    Stream<Table> getAll(Optional<String> dbName, Optional<String> schema, SchemaTableStrategy tableStrategy) throws SQLException;

    /**
     * 某表导出的列
     * @param table
     * @return
     * @throws SQLException
     */
    Stream<Column> getTableColumn(Table table) throws SQLException;

}
