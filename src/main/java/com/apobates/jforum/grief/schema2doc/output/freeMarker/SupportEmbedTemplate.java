package com.apobates.jforum.grief.schema2doc.output.freeMarker;

import com.apobates.jforum.grief.schema2doc.core.SchemaOutput;

/**
 * 嵌入自定义模板的接口
 * 以支持自定义模板功能
 */
@FunctionalInterface
public interface SupportEmbedTemplate {
    /**
     * 嵌入
     * @param template 导出模板
     * @return
     */
    SchemaOutput embed(ExportTemplate template);
}
