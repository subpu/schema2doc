package com.apobates.jforum.grief.schema2doc.reactive;

import com.apobates.jforum.grief.schema2doc.SchemaExportException;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.CompletableFuture;

/**
 * RxJava2 Flowable异步导出
 */
public interface SchemaFlowableOutput {
    /**
     * 输出
     * @param information 基本信息
     * @param table 表格Schema
     * @return 订阅器
     * @throws SchemaExportException
     */
    Disposable flush(Information information, Flowable<Table> table) throws SchemaExportException;

    /**
     * 输出订阅结果状态的Future. 可以据此来判断是否结束
     * @return 若结果是OK表示结束
     */
    CompletableFuture<String> getFuture();
}
