package com.apobates.jforum.grief.schema2doc;

/**
 * 导出异常
 */
public class SchemaExportException extends RuntimeException{
    public SchemaExportException(String message){
        super(message);
    }

    public SchemaExportException(String message, Throwable cause){
        super(message, cause);
    }

    public SchemaExportException(Throwable cause) {
        super(cause);
    }
}
