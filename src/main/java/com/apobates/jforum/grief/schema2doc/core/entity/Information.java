package com.apobates.jforum.grief.schema2doc.core.entity;

/**
 * 基本信息
 */
public class Information {
    private final String title;
    private final String database;
    private final String version;
    private final String description;

    public Information(String title, String database, String version, String description) {
        this.title = title;
        this.database = database;
        this.version = version;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDatabase() {
        return database;
    }

    public String getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Information{" +
                "title='" + title + '\'' +
                ", database='" + database + '\'' +
                ", version='" + version + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
