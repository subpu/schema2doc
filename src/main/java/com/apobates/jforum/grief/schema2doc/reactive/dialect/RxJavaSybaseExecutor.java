package com.apobates.jforum.grief.schema2doc.reactive.dialect;

import com.apobates.jforum.grief.schema2doc.core.SchemaTableStrategy;
import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.core.schema.SchemaDialect;
import com.apobates.jforum.grief.schema2doc.reactive.schema.AbstractRxJavaQueryExecutor;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Optional;
/**
 * RxJava2的Sybase的结果集供应商
 * https://github.com/davidmoten/rxjava-jdbc
 */
public class RxJavaSybaseExecutor extends AbstractRxJavaQueryExecutor {
    private final static Logger logger = LoggerFactory.getLogger(RxJavaSybaseExecutor.class);

    protected RxJavaSybaseExecutor(Database db, SchemaDialect dialect) {
        super(db, dialect);
    }

    @Override
    protected Flowable<Table> getTables(Database db, Optional<String> dbName, Optional<String> schema, SchemaTableStrategy tableStrategy) throws IllegalArgumentException {
        return null;
    }

    @Override
    protected Single<List<Column>> getColumns(Database db, Table table) {
        return null;
    }
}
