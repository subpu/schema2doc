package com.apobates.jforum.grief.schema2doc.core;

import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import java.io.IOException;
import java.util.stream.Stream;

/**
 * 模式输出
 */
@FunctionalInterface
public interface SchemaOutput {
    /**
     * 输出
     * @param information 基本信息
     * @param tables 表格Schema
     * @return 失败的数量
     * @throws IOException
     */
    int flush(Information information, Stream<Table> tables) throws IOException;
}
