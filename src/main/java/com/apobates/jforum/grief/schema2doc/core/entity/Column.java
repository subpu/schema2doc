package com.apobates.jforum.grief.schema2doc.core.entity;

import java.util.Objects;

/**
 * 列信息
 */
public class Column {
    // 列名
    private final String columnName;
    // 字段类型/数据类型
    private final String typeName;
    // 长度/列的长度定义
    private final String columnSize;
    // 是否为空/是否为null/YES?NO
    private final String nullable;
    // 描述/备注
    private final String remarks;
    // 小数位
    private final String decimalDigits;
    // 是否是主键
    private final String primaryKey;
    // 默认值
    private final String columnDef;

    private Column(String columnName, String typeName, String columnSize, Nullable nullable, String remarks, String decimalDigits, PrimayKey pk, String columnDef) {
        this.columnName = columnName;
        this.typeName = typeName;
        this.columnSize = columnSize;
        this.nullable = nullable.name();
        this.remarks = remarks;
        this.decimalDigits = decimalDigits;
        this.primaryKey = (null!=pk)?pk.name():null;
        this.columnDef = columnDef;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getColumnSize() {
        return columnSize;
    }

    public String getNullable() {
        return nullable;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getDecimalDigits() {
        return decimalDigits;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public String getColumnDef() {
        return columnDef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column column = (Column) o;
        return columnName.equals(column.columnName) && typeName.equals(column.typeName) && columnSize.equals(column.columnSize) && nullable.equals(column.nullable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnName, typeName, columnSize, nullable);
    }

    public boolean isBasic(){
        return null == getDecimalDigits() && null == getPrimaryKey() && null == getColumnDef();
    }

    /**
     * 创建基础表结构
     * @param columnName
     * @param typeName
     * @param columnSize
     * @param nullable
     * @param remarks
     * @return
     */
    public static Column basic(String columnName, String typeName, String columnSize, Nullable nullable, String remarks){
        return new Column( columnName,  typeName,  columnSize,  nullable,  remarks, null, null, null);
    }

    /**
     * 将基础表结构扩展为全部
     * @param decimalDigits
     * @param pk
     * @param columnDef
     * @return
     */
    public Column toFull(String decimalDigits, PrimayKey pk, String columnDef){
        if(!this.isBasic()){
            return this;
        }
        return new Column(getColumnName(),  getTypeName(),  getColumnSize(),  Nullable.getInstance(getNullable()),  getRemarks(), decimalDigits, pk, columnDef);
    }

    public Column remold(String columnSize){
        return new Column(getColumnName(),  getTypeName(),  columnSize,  Nullable.getInstance(getNullable()),  getRemarks(), getDecimalDigits(), PrimayKey.getInstance(getPrimaryKey()), getColumnDef());
    }
}
