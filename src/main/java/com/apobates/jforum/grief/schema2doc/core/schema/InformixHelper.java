package com.apobates.jforum.grief.schema2doc.core.schema;

import java.util.HashMap;
import java.util.Map;

public final class InformixHelper {
    private final static Map<Integer,String> DATATYPE;
    static {
        DATATYPE = new HashMap<>();
        String[] seqType = new String[]{
                "CHAR", "SMALLINT", "INTEGER", "FLOAT", "SMALLFLOAT", "DECIMAL", "SERIAL", "DATE", "MONEY", "NULL",
                "DATETIME", "BYTE", "TEXT", "VARCHAR", "INTERVAL", "NCHAR", "NVARCHAR", "INT8", "SERIAL8", "SET", "MULTISET",
                "LIST", "ROW", "COLLECTION"
        };
        for(int index=0;index<seqType.length;index++){
            DATATYPE.put(index, seqType[index]);
        }
        DATATYPE.put(40, "LVARCHAR");
        DATATYPE.put(41, "BLOB");
        DATATYPE.put(42, "LVARCHAR");
        DATATYPE.put(43, "BOOLEAN");
        DATATYPE.put(44, "BIGINT");
        DATATYPE.put(45, "BIGSERIAL");
        DATATYPE.put(256, "CHAR");
        DATATYPE.put(257, "SMALLINT");
        DATATYPE.put(258, "INTEGER");
        DATATYPE.put(259, "FLOAT");
        DATATYPE.put(260, "REAL");
        DATATYPE.put(261, "DECIMAL");
        DATATYPE.put(262, "SERIAL");
        DATATYPE.put(263, "DATE");
        DATATYPE.put(264, "MONEY");
        DATATYPE.put(266, "DATETIME");
        DATATYPE.put(267, "BYTE");
        DATATYPE.put(268, "TEXT");
        DATATYPE.put(269, "VARCHAR");
        DATATYPE.put(270, "INTERVAL");
        DATATYPE.put(271, "NCHAR");
        DATATYPE.put(272, "NVARCHAR");
        DATATYPE.put(273, "INT8");
        DATATYPE.put(274, "SERIAL8");
        DATATYPE.put(275, "SET");
        DATATYPE.put(276, "MULTISET");
        DATATYPE.put(277, "LIST");
        DATATYPE.put(278, "Unnamed ROW");
        DATATYPE.put(296, "LVARCHAR");
        DATATYPE.put(297, "CLOB");
        DATATYPE.put(298, "BLOB");
        DATATYPE.put(299, "BOOLEAN");
        DATATYPE.put(2061, "IDSSECURITYLABEL");
        DATATYPE.put(4118, "ROW");
    }

    public static Map<Integer,String> getDatatype(){
        return DATATYPE;
    }
}
