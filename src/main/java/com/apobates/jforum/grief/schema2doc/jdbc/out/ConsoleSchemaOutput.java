package com.apobates.jforum.grief.schema2doc.jdbc.out;

import com.apobates.jforum.grief.schema2doc.core.SchemaOutput;
import com.apobates.jforum.grief.schema2doc.core.entity.Information;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.github.freva.asciitable.AsciiTable;
import com.github.freva.asciitable.Column;
import com.github.freva.asciitable.OverflowBehaviour;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * 控制台打印
 */
public class ConsoleSchemaOutput implements SchemaOutput {
    private AtomicInteger count = new AtomicInteger(0);

    @Override
    public int flush(Information information, Stream<Table> tables) throws IOException {
        // System.out.println("-------Information-------");
        // System.out.println(information);

        tables.parallel().forEach(table->{
            System.out.println(printAsciiTable(table));
            System.out.println(printAsciiColumns(table.getColumns()));
            System.out.println("\r\n");
            count.addAndGet(1);
        });
        return count.get();
    }

    private static String printAsciiTable(Table table){
        return AsciiTable.getTable(List.of(table), Arrays.asList(
                new Column().header("Table").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getTableName()),
                new Column().header("Remarks").maxWidth(24, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getRemarks()),
                new Column().header("ColumnSize").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(tabIns->table.getColumns().size()+"")
        ));
    }

    private static String printAsciiColumns(List<com.apobates.jforum.grief.schema2doc.core.entity.Column> columns){
        return AsciiTable.getTable(columns, Arrays.asList(
                new Column().header("Name").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnName()),
                new Column().header("Type").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getTypeName()),
                new Column().header("Length").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getColumnSize()),
                new Column().header("Nullable").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getNullable()),
                new Column().header("Remarks").maxWidth(12, OverflowBehaviour.CLIP_RIGHT).with(colIns -> colIns.getRemarks())));
    }
}
