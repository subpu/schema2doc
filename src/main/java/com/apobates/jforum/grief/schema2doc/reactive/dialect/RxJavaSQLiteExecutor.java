package com.apobates.jforum.grief.schema2doc.reactive.dialect;

import com.apobates.jforum.grief.schema2doc.core.SchemaTableStrategy;
import com.apobates.jforum.grief.schema2doc.core.entity.Column;
import com.apobates.jforum.grief.schema2doc.core.entity.Nullable;
import com.apobates.jforum.grief.schema2doc.core.entity.PrimayKey;
import com.apobates.jforum.grief.schema2doc.core.entity.Table;
import com.apobates.jforum.grief.schema2doc.core.schema.SchemaDialect;
import com.apobates.jforum.grief.schema2doc.core.strategy.SchemaTableRegMatchStrategy;
import com.apobates.jforum.grief.schema2doc.reactive.schema.AbstractRxJavaQueryExecutor;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Optional;
/**
 * RxJava2的SQLite的结果集供应商
 * https://github.com/davidmoten/rxjava-jdbc
 */
public class RxJavaSQLiteExecutor extends AbstractRxJavaQueryExecutor {
    private final static Logger logger = LoggerFactory.getLogger(RxJavaSQLiteExecutor.class);

    protected RxJavaSQLiteExecutor(Database db, SchemaDialect dialect) {
        super(db, dialect);
    }

    @Override
    protected Flowable<Table> getTables(Database db, Optional<String> dbName, Optional<String> schema, SchemaTableStrategy tableStrategy) throws IllegalArgumentException {
        if(tableStrategy instanceof SchemaTableRegMatchStrategy){
            logger.warn("SQLite不支持此策略. 原生不支持正则表达式函数");
        }
        // https://www.sqlitetutorial.net/sqlite-describe-table/
        String sql = "SELECT name, rootpage FROM sqlite_schema WHERE type = 'table'";
        if(tableStrategy.sql(SchemaDialect.SQLite).isPresent()){
            sql+= " AND " + tableStrategy.sql(SchemaDialect.SQLite).get().replaceAll(SchemaTableStrategy.placeholder, "name");
        }
        logger.debug("[TABLE-SQLite]execute sql statement::"+sql);
        return db.select(sql).get(record -> {
            // String dbName, String tableName, String remarks
            return Table.noSchema(
                    record.getInt("rootpage"),
                    dbName.get(),
                    record.getString("name"),
                    "remarks");
        }).filter(table->tableStrategy.filter().test(table.getTableName()));
    }

    @Override
    protected Single<List<Column>> getColumns(Database db, Table table) {
        // https://sqldocs.org/sqlite/sqlite-show-table-columns/
        // https://database.guide/5-ways-to-check-a-columns-data-type-in-sqlite/
        // SELECT name,type,`notnull`, dflt_value,pk FROM pragma_table_info('sys_items');
        String sql = "SELECT name, type,`notnull`, dflt_value, pk FROM pragma_table_info('%s')";
        sql = String.format(sql, table.getTableName());
        logger.debug("[COLUMN-SQLite]execute sql statement::"+sql+", Table Arg::"+table.getTableName());
        return db.select(sql).get(record -> {
            // Basic:: String columnName, String typeName, String columnSize, Nullable nullable, String remarks
            String columnName = record.getString("name");
            return Column.basic(
                            columnName,
                            record.getString("type"),
                            "0",
                            Nullable.of(record.getString("notnull").equals("0")?1:2), // 1(NOT NULL/E:NO)/0(NULL/E:YES)
                            "REMARKS")
                    .toFull("0",
                            PrimayKey.of(record.getString("pk").equals("1")?1:2),
                            record.getString("dflt_value")); // String decimalDigits, PrimayKey pk, String columnDef
        }).toList();
    }
}
