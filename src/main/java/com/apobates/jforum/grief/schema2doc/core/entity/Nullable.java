package com.apobates.jforum.grief.schema2doc.core.entity;

/**
 * 是否允许:Null
 */
public enum Nullable {
    YES(1),NO(2);

    private int symbol;


    Nullable(int symbol) {
        this.symbol = symbol;
    }

    public static Nullable of(Integer symbol){
        if(symbol.compareTo(1) == 0){
            return YES;
        }
        return NO;
    }

    public static Nullable getInstance(String names){
        if(names.equals("YES")){
            return YES;
        }
        return NO;
    }
}
