package com.apobates.jforum.grief.schema2doc.core;

import com.apobates.jforum.grief.schema2doc.core.schema.SchemaDialect;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 导出表的策略
 */
public interface SchemaTableStrategy {
    /**
     * 返回匹配表达式, 若存在将直接拼接到查询语句上
     * 注意: 表名的占位符使用: placeholder(%TBCN%)
     * @param dialect 数据方言(内置的)
     * @return
     */
    Optional<String> sql(SchemaDialect dialect);

    /**
     * 返回过滤的谓词函数, 若可用将用在结果的过滤上
     * 注意: 谓词函数的参数是表名
     * @return
     */
    Predicate<String> filter();

    /**
     * 不加限制的表导出策略
     * 相当于导出所有符合条件的表
     * @return
     */
    static SchemaTableStrategy empty(){
        return new SchemaTableStrategy() {
            @Override
            public Optional<String> sql(SchemaDialect dialect) {
                return Optional.empty();
            }

            @Override
            public Predicate<String> filter() {
                return passFilter();
            }
        };
    }

    default Predicate<String> passFilter(){
        return (tableName)->true;
    }

    String placeholder = "%TBCN%";
}
