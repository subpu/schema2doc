module schema2doc {
    requires mysql.connector.java;
    requires org.slf4j;
    requires HikariCP;
    requires java.sql;
    requires org.jooq;
    requires freemarker;
    requires ascii.table;
    requires io.reactivex.rxjava2;
    requires com.github.davidmoten.rxjava2.jdbc;
    requires org.docx4j.core;
    requires org.docx4j.JAXB_MOXy;
    requires org.docx4j.JAXB_ReferenceImpl;
    exports com.apobates.jforum.grief.schema2doc;
    exports com.apobates.jforum.grief.schema2doc.core.entity to freemarker;
    exports com.apobates.jforum.grief.schema2doc.jdbc;
}